# Html generator for perfums

## Prerequisite

Python3 installed.
> -> See here [https://www.python.org/downloads/](https://www.python.org/downloads/).

## Examples test

N/A

## Run the csv_parfums_html_generator.py

```
$ ./csv_parfums_html_generator.py <my_csv_filename>
```

Replacing ```<my_csv_filename>``` by your own perfums CSV file name


## Run the examples test (bash needed !)

```
$ ./examples.sh
```
