#!/usr/bin/env python3

import sys
import os
import csv

################################################################################
# Internal functions

################################################################################
# A) Numéro = 0
# B) Titre du parfum = 1
# C) Classification = 2
# D) Familles olfactives = 3
# E) Description complète = 4
# F) Note(s) parfumée(s) = 5
# G) Mentions et intrédients = 6
# H) Image = 7
# I) <TO_FULFILL> = 8
column_result_letter='I'


def main():
    # 0) Parse argument
    filename = sys.argv[1]

    # 1) Local variable declaration
    html_code = '<hgroup style="background-color:rgba(255, 255, 255, 0.7);' +\
                'padding-left:1rem;padding-right:1rem;padding-top:1rem;padding-bottom:1rem">' +\
                '<h2 class="uppercase"><strong>{number} - {name}<br/></strong></h2>' +\
                '<p style="text-align:justify">{description}</p>' +\
                '<h3>Ingrédients :</h3><p>{ingredients}<br />Contient : {content}</p>' +\
                '<h3>Familles olfactives :</h3><p><b>{main_family}</b>{extra_family}</p>' +\
                '</hgroup>'
    ingredients_str = "Ingrédients : "
    content_str = 'Contient : '

    # 2) Core processing

    # Open the files
    file_in = open(filename, "r")
    try:
        os.mkdir("out")
    except:
        pass
    file_out = open("out/" + os.path.basename(filename), "w")
    # Convert it as csv object
    csvreader = csv.reader(file_in)
    # Get first row (header)
    header = next(csvreader)
    # Convert it as csv writter
    csvwriter = csv.writer(file_out)
    # Get others row (data)
    rows = [row for row in csvreader]
    rows = rows
    # Close reader
    file_in.close()

    # Load datas
    number_list = [row[0] for row in rows]
    name_list = [row[1] for row in rows]
    family_list = [row[3] for row in rows]
    description_list = [row[4] for row in rows]
    legal_list = [row[6] for row in rows]

    # Iterate over the perfums number
    for idx, number in enumerate(rows):
        #import pdb; pdb.set_trace()
        ingredients_start = legal_list[idx].find(ingredients_str[:-3]) + len(ingredients_str) - 1
        ingredients_end = legal_list[idx].find(content_str[:-3])
        content_start = ingredients_end + len(content_str) - 1
        _family_index = family_list[idx].find(',') if family_list[idx].find(',') != -1 else len(family_list[idx])
        formatted_html = \
            html_code.format(number=number_list[idx],
            name=name_list[idx],
            description=description_list[idx].replace('\n',''),
            ingredients=legal_list[idx][ingredients_start:ingredients_end],
            content=legal_list[idx][content_start:],
            main_family=family_list[idx][:_family_index].title(),
            extra_family=family_list[idx][_family_index:].title())
        rows[idx][8] = formatted_html

    csvwriter.writerow(header)
    csvwriter.writerows(rows)
    file_out.close()
    print("%s code have been successfully generated in column %c" \
          % (filename, column_result_letter))

main()
